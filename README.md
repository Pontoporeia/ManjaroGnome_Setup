# dotefiles

Set of shell scripts to automatically install my prefered desktop enviroments in an Archlinux-like environment. 

These scripts are run after a fresh:
- Manjaro Gnome installation.
- EndeavourOS Gnome installation.
- Archlinux Gnome installation.
- config files for hyprland/waybar/fuzzel/kitty installation

Refactoring in progress. 

## TODO

- [ ] Work in progress to differantiate **use case** sets of apps, ex: Designer, Dev, Writing, etc in `scripts/flatpak.sh`
- [ ] Cleanup `pacman.sh` to make it distro agnostic // arch-based
- [X] clear current dotefiles and push current `.bashrc`

## Archive

Paste in terminal to download script and launch immediatly

### `yay` installer

```
sh -c "$(curl -fsSL https://codeberg.org/Pontoporeia/dotfiles/raw/branch/main/scripts/install_yay.sh)"
```

#### Full setup : 

```
sh -c "$(curl -fsSL https://codeberg.org/Pontoporeia/ManjaroGnome_Setup/raw/branch/main/scripts/main.sh)"
```
Or individual scripts for :

#### Pacman setup: 

```
sh -c "$(curl -fsSL https://codeberg.org/Pontoporeia/ManjaroGnome_Setup/raw/branch/main/scripts/pacman.sh)"
```

#### Flatpak setup:

```
sh -c "$(curl -fsSL https://codeberg.org/Pontoporeia/ManjaroGnome_Setup/raw/branch/main/scripts/flatpak.sh)"
```

#### Lamp setup:

```
sh -c "$(curl -fsSL https://codeberg.org/Pontoporeia/ManjaroGnome_Setup/raw/branch/main/scripts/lamp.sh)"
```

!! (Individual scripts do not fetch dotfiles)


#### [Chevec's Flatpak Gaming on Linux script](https://github.com/Chevek/Gaming-Flatpak)

```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/Chevek/Gaming-Flatpak/main/gaming-flatpak.sh)"
```