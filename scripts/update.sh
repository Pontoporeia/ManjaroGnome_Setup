#!/usr/bin/env bash

set -o noclobber  # Avoid overlay files (printf "hi" > foo)
set -o errexit    # Used to exit upon error, avoiding cascading errors
set -o pipefail   # Unveils hidden failures
set -o nounset    # Exposes unset variables
printf "\n --> 🎯System will be updated to latest archlinux, AUR and flatpak versions 📦\n"
printf "\n Updating Archlinux packages ...\n"
sudo pacman -Syyu
printf "\nUpdating AUR packages ...\n"
yay -Syu
printf "\nUpdating Flatpak packages ...\n"
flatpak update

printf "\n --> ✅ System updated to latest archlinux, AUR and flatpak versions 📦\n" 
