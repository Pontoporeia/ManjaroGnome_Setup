#!/bin/bash

set -euf -o pipefail

sudo pacman -Syyu --noconfirm
echo "~> Installing LAMP packages..."
sudo pacman -S --noconfirm apache mysql php php-apache phpmyadmin
echo ""
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql



sed -i '/LoadModule unique_id_module modules\/mod_unique_id.so/s/^#//g' /etc/httpd/conf/httpd.conf
sed -i '/LoadModule mpm_event_module modules\/mod_mpm_event.so/s/^/#/g' /etc/httpd/conf/httpd.conf
sed -i '/LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so/s/^#//g' /etc/httpd/conf/httpd.conf

# TODO find out why it won't operate corretly in this case...
sed -i 'extension=bz2/s/^;//g' /etc/php/php.ini
sed -i 'extension=mysqli/s/^;//g' /etc/php/php.ini

sudo tee -a /etc/httpd/conf/extra/phpmyadmin.conf > /dev/null <<EOT
Alias /phpmyadmin "/usr/share/webapps/phpMyAdmin"
<Directory "/usr/share/webapps/phpMyAdmin">
DirectoryIndex index.php
AllowOverride All
Options FollowSymlinks
Require all granted
</Directory>
EOT

sudo tee -a /etc/httpd/conf/httpd.conf > /dev/null <<EOT
LoadModule php_module modules/libphp.so
AddHandler php-script php
Include conf/extra/php_module.conf
Include conf/extra/phpmyadmin.conf
EOT

sudo systemctl enable mysqld
sudo systemctl start mysqld
sudo systemctl enable httpd
sudo systemctl restart httpd

sudo mysql_secure_installation
