#!/bin/bash
set -euf -o pipefail

INSTALL=(

    yay
    appimagelauncher
    xapp-appimage-thumbnailer
    gnome-epub-thumbnailer

    terminator
    micro
    neovim
    figlet
    tree
    exa
    yt-dlp
    fontconfig

    tor

    python-pip

    docker
    docker-compose

    nodejs
    npm

    caddy

    webapp-manager

    # manjaro-pipewire
    # easyeffects
    alsa-utils
    mediainfo
    # gnome-layout-switcher
    webp-pixbuf-loader
    nautilus-image-converter

    # esthetics for Gnome
    kvantum
    qt5ct
    qt6ct

    # Specific non-flatpak software
    # blender
    # minder

    # Document libs
    pandoc
    texlive-bin
    texlive-core
)

REMOVE=(
    gedit
    fragments
    gnome-terminal
    lollypop
    gnome-chess
    geary
    gnome-calculator
    gnome-boxes
    gnome-calendar
    gnome-characters
    cheese
    gnome-clocks
    gnome-contacts
    deja-dup
    baobab
    firefox
    firefox-gnome-theme-maia
    gnome-mines
    gthumb
    alacarte
    gnome-todo
    totem
    gnome-weather 
    gnome-font-viewer
    gnome-chess
)


echo "~~> Fetching fastest pacman mirror"
sudo pacman-mirrors --fasttrack

echo "~~> Adding SSD timer service"
sudo systemctl enable fstrim.timer
sudo systemctl start fstrim.timer

echo "~~> Starting ufw firewall"
sudo pacman -S --noconfirm ufw
sudo ufw enable

echo "~~> Upgrading system"
sudo pacman -Syyu --noconfirm

echo "~~> Installing alternate utilities & other apps"
for app in "${INSTALL[@]}"; do
    if pacman -Qs "$app" > /dev/null ; then
        continue
    else
        sudo pacman -S --noconfirm "$app" > /dev/null
    fi 
done

echo "~~> Removing default apps"
for app in "${REMOVE[@]}"; do
    if pacman -Qs "$app" > /dev/null ; then
        sudo pacman -R --noconfirm "$app" > /dev/null
    else
        continue
    fi 
done

echo "~~> Adding Terminal custom shortcut"
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Terminal"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "terminator"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Ctrl><Alt>t"

echo "~~> Fixing alsamixer weird mute default"
amixer -c 1 set 'Auto-Mute Mode' Disabled

echo "~~> Fixing Vscodium Folder issue"
echo "inode/directory=org.gnome.Nautilus.desktop" >> ~/.config/mimeapps.list

echo "~~> Installing AUR apps"
yay -s popcorntime-bin lite-xl

