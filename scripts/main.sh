#!/bin/bash

set -euf -o pipefail

echo "==> Manjaro fresh install setup... "
sh -c "$(curl -fsSL https://codeberg.org/Pontoporeia/ManjaroGnome_Setup/raw/branch/main/manjaro_fresh.sh)"
echo "==> Installing flatpak packages from flatpak_apps_setup.sh ..." 
sh -c "$(curl -fsSL https://codeberg.org/Pontoporeia/ManjaroGnome_Setup/raw/branch/main/flatpak_apps_setup.sh)"
echo "==> Installing Oh-My-Zsh and Powerlevel10k..."
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}"/themes/powerlevel10k

curl -sO https://codeberg.org/Pontoporeia/ManjaroGnome_Setup/raw/branch/main/dotefiles/.p10k.zsh
curl -sO https://codeberg.org/Pontoporeia/ManjaroGnome_Setup/raw/branch/main/dotefiles/.zshrc


