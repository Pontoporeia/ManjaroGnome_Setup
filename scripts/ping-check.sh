#!/usr/bin/env bash
# TODO: setup script with argument version
# TODO: search other sorts of checks that can be done to emprove troubleshooting.
sites=("fgm.happyngreen.fr" "synapse.happyngreen.fr" "jelly.happyngreen.fr" "ether.happyngreen.fr" "tgm.happyngreen.fr" "tgmp.happyngreen.fr")
unreachable="tgmf.happyngreen.fr"
printf "\nChecking following websites...\n"
for p in "${sites[@]}"; do
  printf "\n$p\n"
done
printf "\n$unreachable -- check for unreachable\n"
printf "\n============\n\n"
for p in "${sites[@]}"; do
  ping -q -c 5 "$p"
  if [[ $? -eq 0 ]]; then
    printf " - [OK] $p is reachable.\n\n"
  else
    printf " - [ERROR] $p is not reachable.\n\n"
  fi
done
ping -q -c 5 "$unreachable"
  if [[ $? -eq 0 ]]; then
    printf " - [OK] $p is reachable.\n\n"
  else
    printf " - [ERROR] $p is not reachable.\n\n"
  fi
