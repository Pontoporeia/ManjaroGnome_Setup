#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

if [[ "${TRACE-0}" == "1" ]]; then
	set -o xtrace
fi

if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
	echo 'Usage: ./flatpak.sh (-s) 

This is my personnal script to install flatpaks I use in the Gnome Desktop Environment. 
Apps are categories in a couple of categories, and at some point I will setup argument parsing or flags in this script to be able to select what categories to install. 
			-s : installs flatpaks System wide. If not declared, then it will install for the user only. 

'
	exit
fi

cd "$(dirname "$0")"

main() {

	#TODO: add pacman specific installs and maybe have an auto onliner to get bash scripts and auto run scripts
	#Depots flatapk
	# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	# flatpak remote-add --if-not-exists --user flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
	# flatpak update --appstream

	# echo "____Gnome apps..."
	GNOME=(
		org.gnome.Boxes
		org.gnome.Boxes.Extension.OsinfoDb
		org.gnome.Calculator
		org.gnome.Calendar
		org.gnome.Characters
		org.gnome.Cheese
		org.gnome.Connections
		org.gnome.Contacts
		org.gnome.Dictionary
		org.gnome.Epiphany
		org.gnome.Evince
		org.gnome.FontManager
		org.gnome.Glade
		org.gnome.Keysign
		org.gnome.Logs
		org.gnome.Maps
		org.gnome.PowerStats
		org.gnome.Solanum
		org.gnome.SoundRecorder
		org.gnome.TextEditor
		org.gnome.Todo
		org.gnome.Weather
		org.gnome.World.Secrets
		org.gnome.clocks
		org.gnome.design.Contrast
		org.gnome.design.IconLibrary
		org.gnome.design.Palette
		org.gnome.eog
		org.gnome.font-viewer
		org.gnome.gitlab.YaLTeR.Identity
		org.gnome.gitlab.cheywood.Iotas
	)

	# echo "____Config apps..."
	CONFIG=(
		org.gtk.Gtk3theme.adw-gtk3
		org.gtk.Gtk3theme.adw-gtk3-dark
		com.github.sdv43.whaler
		org.gnome.baobab
		org.gnome.DejaDup
		net.mediaarea.MediaInfo
		com.mattjakeman.ExtensionManager
		io.github.realmazharhussain.GdmSettings
		io.github.arunsivaramanneo.GPUViewer
		com.mattjakeman.ExtensionManager
		com.github.tchx84.Flatseal
		io.github.realmazharhussain.GdmSettings
		com.github.marhkb.Pods
		com.leinardi.gst
		io.github.mpobaschnig.Vaults
	)

	# echo "____Messaging apps..."
	BROWSER=(
		org.mozilla.firefox
		com.github.Eloston.UngoogledChromium
	)

	# echo "____Messaging apps..."
	MESSAGING=(
		org.mozilla.Thunderbird
		org.signal.Signal
		im.riot.Riot
		com.discordapp.Discord
		com.gitlab.newsflash
		dev.atoft.Reactions
		chat.delta.desktop
		chat.revolt.RevoltDesktop
		org.briarproject.Briar
		org.gnome.Fractal
	)

	# echo "____Design apps..."
	DESIGN=(
		net.scribus.Scribus
		org.inkscape.Inkscape
		org.kde.krita
		org.gimp.GIMP
		org.gnome.design.Palette
		nl.hjdskes.gcolor3
		io.github.lainsce.Emulsion
		io.github.lainsce.Colorway
		org.gabmus.swatch
		io.github.nate_xyz.Paleta
		com.github.finefindus.eyedropper
		fr.natron.Natron
		org.mypaint.MyPaint
		com.github.huluti.Curtail
		org.darktable.Darktable
		org.gnome.design.Contrast
		org.gustavoperedo.FontDownloader
		com.rafaelmardojai.WebfontKitGenerator
		com.boxy_svg.BoxySVG
		io.gitlab.adhami3310.Converter
		io.gitlab.theevilskeleton.Upscaler
		io.github.nate_xyz.Conjure
	)

	# echo "____Multimedia apps..."
	MULTIMEDIA=(
		com.github.rafostar.Clapper
		info.smplayer.SMPlayer
		com.github.iwalton3.jellyfin-media-player
		io.freetubeapp.FreeTube
		com.spotify.Client
		org.kde.kdenlive
		org.olivevideoeditor.Olive
		com.obsproject.Studio
		com.github.geigi.cozy
		fr.handbrake.ghb
		org.audacityteam.Audacity
		org.nickvision.tubeconverter
		org.blender.Blender
		de.haeckerfelix.Shortwave
		org.pulseaudio.pavucontrol
		com.github.wwmm.easyeffects
		org.pipewire.Helvum
		app.drey.EarTag
		app.drey.Dialect
		io.bassi.Amberol
		com.github.neithern.g4music
		com.github.unrud.VideoDownloader
		io.gitlab.zehkira.Monophony

	)

	# echo "____Developpment apps..."
	DEV=(
		org.gnome.Builder
		com.vscodium.codium
		re.sonny.Commit
		re.sonny.Workbench
		org.gnome.Glade
		org.gnome.Devhelp
		org.gnome.Keysign
		org.filezillaproject.Filezilla
		org.gnome.design.IconLibrary
		com.github.sdv43.whaler
		org.godotengine.Godot
		re.sonny.Workbench
		org.gnome.Devhelp
	)

	# echo "____Gaming apps..."
	GAMING=(
		com.valvesoftware.Steam
		com.valvesoftware.Steam.CompatibilityTool.Proton-GE
		io.github.Foldex.AdwSteamGtk
		net.davidotek.pupgui2
		org.freedesktop.Platform.VulkanLayer.MangoHud
		# org.openrgb.OpenRGB
		com.leinardi.gst
		io.github.antimicrox.antimicrox
		com.usebottles.bottles
		com.heroicgameslauncher.hgl
		net.lutris.Lutris
		org.winehq.Wine.DLLs.dxvk
		org.winehq.Wine.gecko
		org.winehq.Wine.mono
	)

	# echo "____Office apps..."
	OFFICE=(
		com.bitwarden.desktop
		org.libreoffice.LibreOffice
		io.posidon.Paper
		org.zotero.Zotero
		com.bitwarden.desktop
		fyi.zoey.Boop-GTK
		io.github.leonardschardijn.Chirurgien
		io.github.lainsce.Countdown
		com.github.jeromerobert.pdfarranger
		com.logseq.Logseq
		com.github.johnfactotum.Foliate
		md.obsidian.Obsidian
		net.cozic.joplin_desktop
		com.github.tenderowl.frog
		app.drey.Dialect
		com.github.flxzt.rnote
		net.danigm.timetrack
		com.notepadqq.Notepadqq
		com.nextcloud.desktopclient.nextcloud
		com.hunterwittenborn.Celeste
		com.gitlab.newsflash
		com.belmoussaoui.ReadItLater
		com.calibre_ebook.calibre
		io.github.troyeguo.koodo-reader
		info.febvre.Komikku
		io.github.lainsce.Notejot
	)

	# echo "____Utility apps..."
	UTILITY=(
		org.freefilesync.FreeFileSync
		com.lakoliu.Furtherance
		com.belmoussaoui.Decoder
		com.github.liferooter.textpieces
		dev.geopjr.Collision
		net.danigm.timetrack
		io.github.mpobaschnig.Vaults
		com.gitlab.cunidev.Workflow
		com.github.johnfactotum.QuickLookup
		org.gnome.GHex
		fr.romainvigier.MetadataCleaner
		com.belmoussaoui.Obfuscate
		org.gabmus.whatip
		org.x.Warpinator
		com.vixalien.sticky
		com.belmoussaoui.Decoder
		com.belmoussaoui.Obfuscate
		com.github.qarmin.czkawka
		com.github.qarmin.szyszka
		io.github.lainsce.Countdown
		com.raggesilver.BlackBox
		io.github.jorchube.monitorets
		com.gitlab.cunidev.Workflow
		com.github.vikdevelop.timer
	)

	#____Torrent apps

	TORRENT=(
		com.transmissionbt.Transmission
		org.deluge_torrent.deluge
		org.qbittorrent.qBittorrent
		com.stremio.Stremio
	)

	LISTS=(
		"${GNOME[@]}"
		"${CONFIG[@]}"
		"${BROWSER[@]}"
		"${MESSAGING[@]}"
		"${DESIGN[@]}"
		"${MULTIMEDIA[@]}"
		"${DEV[@]}"
		"${GAMING[@]}"
		"${OFFICE[@]}"
		"${UTILITY[@]}"
		"${TORRENT[@]}"
		# "${TOOL}"
		# "${TOOL}"
		# "${TOOL}"
	)

	for LIST_ITEM in "${LISTS[@]}"; do
		for app in "${LIST_ITEM[@]}"; do
			# TODO add flag parsing for --system
			flatpak install flathub -y --noninteractive "$app"
		done
	done

	# ___________________Tweaks

	# TODO: refactor .themes copy
	# add gtk-themes and kvantum themes pull from github
	# https://github.com/lassekongo83/adw-gtk3/releases/latest
	# git clone https://github.com/GabePoel/KvLibadwaita.git
	# cd KvLibadwaita
	# ./install.sh

	# echo "~> Fixing flatpak themes issue"
	# if [ ! -d "$HOME/.themes" ]; then
	# mkdir ~/.themes
	# cp -r /usr/share/themes/* ~/.themes
	# sudo flatpak override --filesystem="$HOME"/.themes
	# else
	# if find "$HOME/.themes" -mindepth 1 -maxdepth 1 | read -r ; then
	# true
	# else
	# cp -r /usr/share/themes/* ~/.themes
	# sudo flatpak override --filesystem="$HOME"/.themes
	# fi
	# fi

	echo "### installation complete :D"
}

main "$@"
